drop database dozee;

create database dozee;

use dozee;

CREATE TABLE `devices` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `uuid` VARCHAR(255) DEFAULT NULL,
    `name` VARCHAR(255) DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `uuid_UNIQUE` (`uuid`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `users` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `first_name` VARCHAR(255) DEFAULT NULL,
    `last_name` VARCHAR(255) DEFAULT NULL,
    `email` VARCHAR(255) DEFAULT NULL,
    `password` VARCHAR(255) DEFAULT NULL,
    `device_id` INT(11) DEFAULT NULL,
    `created_at` TIMESTAMP NULL DEFAULT NULL,
    `updated_at` TIMESTAMP NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `email_UNIQUE` (`email`),
    KEY `users_fk_device_id_idx` (`device_id`),
    CONSTRAINT `users_fk_device_id` FOREIGN KEY (`device_id`)
        REFERENCES `devices` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `user_devices` (
  `user_id` int(11) DEFAULT NULL,
  `device_id` int(11) DEFAULT NULL,
  KEY `user_devices_fk_user_id_idx` (`user_id`),
  KEY `user_devices_fk_device_id_idx` (`device_id`),
  CONSTRAINT `user_devices_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `user_devices_fk_device_id` FOREIGN KEY (`device_id`) REFERENCES `devices` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sleeps` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `device_id` INT(11) DEFAULT NULL,
    `bed_time` DATETIME DEFAULT NULL,
    `sleep_time` DATETIME DEFAULT NULL,
    `wakeup_time` DATETIME DEFAULT NULL,
    `heart_rate` SMALLINT(6) DEFAULT NULL,
    `breath_rate` SMALLINT(6) DEFAULT NULL,
    `humidity` SMALLINT(6) DEFAULT NULL,
    `temperature` SMALLINT(6) DEFAULT NULL,
    `noise` SMALLINT(6) DEFAULT NULL,
    `sleep_score` SMALLINT(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `wakeup_time_idx` (`wakeup_time`)
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `ambiences` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `sleep_id` INT(11) DEFAULT NULL,
    `timestamp` DATETIME DEFAULT NULL,
    `temperature` SMALLINT(6) DEFAULT NULL,
    `humidity` SMALLINT(6) DEFAULT NULL,
    `noise` SMALLINT(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `ambiences_fk_sleeps_id_idx` (`sleep_id`),
    CONSTRAINT `ambiences_fk_sleeps_id` FOREIGN KEY (`sleep_id`)
        REFERENCES `sleeps` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;

CREATE TABLE `vitals` (
    `id` INT(11) NOT NULL AUTO_INCREMENT,
    `sleep_id` INT(11) DEFAULT NULL,
    `timestamp` DATETIME DEFAULT NULL,
    `heart_rate` SMALLINT(6) DEFAULT NULL,
    `breath_rate` SMALLINT(6) DEFAULT NULL,
    PRIMARY KEY (`id`),
    KEY `vitals_fk_sleep_id_idx` (`sleep_id`),
    CONSTRAINT `vitals_fk_sleep_id` FOREIGN KEY (`sleep_id`)
        REFERENCES `sleeps` (`id`)
        ON DELETE NO ACTION ON UPDATE NO ACTION
)  ENGINE=INNODB DEFAULT CHARSET=UTF8;
