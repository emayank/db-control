var Sequelize = require('sequelize');

var db = {
    database: 'dozee',
    username: 'root',
    password: 'nahinhai',
    options: {
        host: 'localhost',
        dialect: 'mysql',
    }
}

var sequelize = new Sequelize(db.database, db.username, db.password, db.options);
module.exports = {
    db: db,
    sequelize: sequelize
}