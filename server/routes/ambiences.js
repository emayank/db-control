var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Ambiences = models.Vitals;
var Sleeps = models.Sleeps;

router.post('/:sleepId', function (req, res, next) {
  var ambienceInfo = req.body;
  Ambiences.bulkCreate(ambienceInfo).then(function () {
    res.send({ status: 'success' });
  });
});

router.get('/:sleepId', function (req, res, next) {
  var sleepId = req.params.sleepId;
  Sleeps.findById(sleepId).then(function (sleep) {
    sleep.getAmbiences().then(function (ambiences) {
      res.send(ambiences);
    });
  });
});

module.exports = router;
