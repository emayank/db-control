var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Users = models.Users;
var Devices = models.Devices;
var UserDevices = models.UserDevices;

router.post('/', function (req, res, next) {
  var userInfo = req.body;
  var deviceInfo = { uuid: userInfo.deviceUuid };
  delete userInfo.deviceUuid;
  Devices.create(deviceInfo).then(function (device) {
    userInfo.deviceId = device.id;
    Users.create(userInfo).then(function (user) {
      userDeviceInfo = { userId: user.id, deviceId: device.id };
      UserDevices.create(userDeviceInfo).then(function (userDevice) {
        res.send({ status: 'success' });
      });
    });
  });
});

router.put('/:id', function (req, res, next) {
  res.send('respond with a resource');
});

router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  Users.findById(id, { attributes: ['email', 'firstName', 'lastName', 'deviceId'] }).then(function (user) {
    if (user) {
      Devices.findById(user.deviceId, { attributes: ['id', 'uuid'] }).then(function (device) {
        var response = user.dataValues;
        response.device = device.dataValues;
        res.send(response);
      });
    } else {
      res.send({});
    }
  });
});

module.exports = router;
