var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Users = models.Users;
var UserDevices = models.UserDevices;

router.post('/:userId/:deviceId', function (req, res, next) {
  var userId = req.params.userId;
  var deviceId = req.params.deviceId;
  var userDeviceInfo = { userId: userId, deviceId: deviceId };
  UserDevices.create(userDeviceInfo).then(function (userDevice) {
    res.send({ status: 'success' });
  });
});

router.put('/:userId/:deviceId', function (req, res, next) {
  var userId = req.params.userId;
  var deviceId = req.params.deviceId;
  var userDeviceInfo = { userId: userId, deviceId: deviceId };
  var userDeviceUpdate = req.body;
  UserDevices.update(userDeviceUpdate, { where: userDeviceInfo }).then(function (userDevices) {
    res.send({ status: 'success' });
  });
});

router.get('/:userId', function (req, res, next) {
  var userId = req.params.userId;
  Users.findById(userId).then(function (user) {
    user.getUserDevices().then(function (userDevices) {
      res.send(userDevices);
    });
  });
});

router.delete('/:userId/:deviceId', function (req, res, next) {
  res.send('respond with a resource');
});

module.exports = router;
