var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Sleeps = models.Sleeps;

router.post('/', function (req, res, next) {
  var sleepInfo = req.body;
  Sleeps.create(sleepInfo).then(function (sleep) {
    res.send({ status: 'success' });
  });
});

router.put('/:id', function (req, res, next) {
  res.send('respond with a resource');
});

router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  Sleeps.findById(id).then(function (sleep) {
    if (sleep) {
      res.send(sleep.dataValues);
    } else {
      res.send({});
    }
  });
});

router.get('/:deviceId/:limit', function (req, res, next) {
  var deviceId = req.params.deviceId;
  var limit = req.params.limit;
  Sleeps.findAll({ where: { deviceId: deviceId }, limit: limit }).then(function (sleeps) {
    res.send(sleeps);
  });
});

router.get('/:deviceId/:timestamp/:limit', function (req, res, next) {
  var deviceId = req.params.deviceId;
  var timestamp = req.params.timestamp;
  var limit = req.params.limit;
  Sleeps.findAll({ where: { wakeupTime: { $lt: timestamp } }, limit: limit }).then(function (sleeps) {
    res.send(sleeps);
  });
});

module.exports = router;
