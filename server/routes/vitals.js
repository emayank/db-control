var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Vitals = models.Vitals;
var Sleeps = models.Sleeps;

router.post('/:sleepId', function (req, res, next) {
  var vitalInfo = req.body;
  Vitals.bulkCreate(vitalInfo).then(function () {
    res.send({ status: 'success' });
  });
});

router.get('/:sleepId', function (req, res, next) {
  var sleepId = req.params.sleepId;
  Sleeps.findById(sleepId).then(function (sleep) {
    sleep.getVitals().then(function (vitals) {
      res.send(vitals);
    });
  });
});

module.exports = router;
