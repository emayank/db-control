var express = require('express');
var models = require('../schema/models');
var router = express.Router();

var Devices = models.Devices;

router.post('/', function (req, res, next) {
  var deviceInfo = req.body;
  Users.create(deviceInfo).then(function (device) {
    res.send({ status: 'success' });
  });
});

router.put('/:id', function (req, res, next) {
  res.send('respond with a resource');
});

router.get('/:id', function (req, res, next) {
  var id = req.params.id;
  Devices.findById(id).then(function (device) {
    if (device) {
      res.send(device.dataValues);
    } else {
      res.send({});
    }
  });
});

module.exports = router;
