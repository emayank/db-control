var Sequelize = require('sequelize');
var config = require('../etc/config');
var sequelize = config.sequelize;

var Users = sequelize.define('users', {
  deviceId: { type: Sequelize.INTEGER, field: 'device_id' },
  email: { type: Sequelize.STRING, unique: true, field: 'email' },
  password: { type: Sequelize.STRING, field: 'password' },
  firstName: { type: Sequelize.STRING, field: 'first_name' },
  lastName: { type: Sequelize.STRING, field: 'last_name' }
}, {
    underscored: true
  });

var Devices = sequelize.define('devices', {
  uuid: Sequelize.STRING,
  name: Sequelize.STRING
}, {
    underscored: true
  });

// Devices.hasOne(Users);

var UserDevices = sequelize.define('user_devices', {
  userId: { type: Sequelize.INTEGER, field: 'user_id' },
  deviceId: { type: Sequelize.INTEGER, field: 'device_id' }
}, {
    timestamps: false,
    underscored: true
  });

UserDevices.removeAttribute('id');
Users.hasMany(UserDevices, {as: 'UserDevices'});

var Sleeps = sequelize.define('sleeps', {
  deviceId: { type: Sequelize.INTEGER, field: 'device_id' },
  bedTime: { type: Sequelize.DATE, field: 'bed_time' },
  sleepTime: { type: Sequelize.DATE, field: 'sleep_time' },
  wakeupTime: { type: Sequelize.DATE, field: 'wakeup_time' },
  heartRate: { type: Sequelize.INTEGER, field: 'heart_rate' },
  breathRate: { type: Sequelize.INTEGER, field: 'breath_rate' },
  humidity: { type: Sequelize.INTEGER, field: 'humidity' },
  temperature: { type: Sequelize.INTEGER, field: 'temperature' },
  noise: { type: Sequelize.INTEGER, field: 'noise' },
  sleepScore: { type: Sequelize.INTEGER, field: 'sleep_score' }
}, {
    timestamps: false,
    underscored: true
  });

Devices.hasMany(Sleeps);

var Ambiences = sequelize.define('ambiences', {
  sleepId: { type: Sequelize.INTEGER, field: 'sleep_id' },
  timestamp: { type: Sequelize.DATE, field: 'timestamp' },
  humidity: { type: Sequelize.INTEGER, field: 'humidity' },
  temperature: { type: Sequelize.INTEGER, field: 'temperature' },
  noise: { type: Sequelize.INTEGER, field: 'noise' }
}, {
    timestamps: false,
    underscored: true
  });

Sleeps.hasMany(Ambiences);

var Vitals = sequelize.define('vitals', {
  sleepId: { type: Sequelize.INTEGER, field: 'sleep_id' },
  timestamp: { type: Sequelize.DATE, field: 'timestamp' },
  heartRate: { type: Sequelize.INTEGER, field: 'heart_rate' },
  breathRate: { type: Sequelize.INTEGER, field: 'breath_rate' }
}, {
    timestamps: false,
    underscored: true
  });

Sleeps.hasMany(Vitals);

module.exports = {
  Users: Users,
  Devices: Devices,
  UserDevices: UserDevices,
  Sleeps: Sleeps
}